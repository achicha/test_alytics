## test alytics

### install:
    pip install -r requirements.txt
    sudo apt-get install rabbitmq-server

### config:
    
    #rabbitmq
    sudo rabbitmqctl add_user myuser mypassword
    sudo rabbitmqctl add_vhost myvhost
    sudo rabbitmqctl set_user_tags myuser mytag
    sudo rabbitmqctl set_permissions -p myvhost myuser ".*" ".*" ".*"

    #django
    python manage.py makemigrations
    python manage.py migrate

### run
    
    #virtualenv
    source venv/bin/activate
    
    #django
    python manage.py runserver
    
    #celery
    celery multi start 4 -A test_aly  -Q:1 first -Q:2 second -Q:3 third -Q:4 celery --loglevel=INFO --logfile=celeryd.${USER}%n.log

    #flower
    celery flower --broker='amqp://myuser:mypassword@localhost:5672/myvhost'
    
### links
    
-----------------------------------------------------    
|main page   | http://127.0.0.1:8000/               
|:-----------:|:------------------------------------:|
|datasets| [http://127.0.0.1:8000/api/v1/arrays/](http://127.0.0.1:8000/api/v1/arrays/) 
|errors|[http://127.0.0.1:8000/api/v1/errors/](http://127.0.0.1:8000/api/v1/errors/)
|single dataset| http://127.0.0.1:8000/api/v1/arrays/<id_number>
|add new data|[http://127.0.0.1:8000/api/v1/arrays/new](http://127.0.0.1:8000/api/v1/arrays/new)
|flower|[http://127.0.0.1:5555/dashboard](http://127.0.0.1:5555/dashboard)

### how it looks like
![all datasets](./pics/datasets.png "Datasets")
![add new data](./pics/new_data.png "add new data")
![errors](./pics/errors.png "Errors")
![flower](./pics/flower.png "Flower")

    