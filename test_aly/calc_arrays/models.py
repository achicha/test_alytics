from django.db import models
from django.contrib.postgres.fields import JSONField


# Create your models here.


class Arrays(models.Model):
    """
    NumberArrays Model, where we store our datasets
    """
    class Meta:
        db_table = "number_arrays"

    id = models.AutoField(primary_key=True)
    stored_array = JSONField(blank=True, null=True, default=list([]))
    result = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=True)
    updated_at = models.DateTimeField(auto_now=True, editable=True)


class Errors(models.Model):
    """Errors Model, keep all exceptions from calculations"""

    class Meta:
        db_table = "errors"

    id = models.AutoField(primary_key=True)
    error_stack = models.TextField(blank=True, null=True)
    arrays = models.ForeignKey(Arrays)                      # FK from Arrays Model
