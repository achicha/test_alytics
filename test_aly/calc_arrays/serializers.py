from rest_framework import serializers
from .models import Arrays, Errors


class ArraysSerializer(serializers.ModelSerializer):
    class Meta:
        model = Arrays
        fields = ('stored_array', 'result')


class ErrorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Errors
        fields = ('arrays', 'error_stack')
