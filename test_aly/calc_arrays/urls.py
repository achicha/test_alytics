from django.conf.urls import url
from . import views


urlpatterns = [
    # url(
    #     r'^api/v1/arrays/(?P<pk>[0-9]+)$/result',
    #     views.get_array_result,
    #     name='get_array_result'
    # ),
    url(
        r'^api/v1/arrays/(?P<pk>[0-9]+)$',
        views.get_single_array,
        name='get_single_array'
    ),
    url(
        r'^api/v1/arrays/new$',
        views.post_new_array,
        name='post_new_array'
    ),
    url(
        r'^api/v1/arrays/$',
        views.get_all_arrays,
        name='get_all_arrays'
    ),
    url(
        r'^api/v1/errors/(?P<pk>[0-9]+)$',
        views.get_single_array,
        name='get_single_array_error'
    ),
    url(
        r'^api/v1/errors/$',
        views.get_errors,
        name='get_errors'
    ),
    url(
        r'^$',
        views.index,
        name='index'
    )
]
