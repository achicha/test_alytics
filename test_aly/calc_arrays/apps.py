from django.apps import AppConfig


class CulcNumbersConfig(AppConfig):
    name = 'culc_numbers'
