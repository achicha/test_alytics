import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import Arrays
from ..serializers import ArraysSerializer


# initialize the APIClient app
client = Client()


class NumberArraysViewsTest(TestCase):
    """ Test module for GET all puppies API """

    def setUp(self):
        self.first = Arrays.objects.create(
            stored_array=[{
                "a": 1,
                "b": 2
            }]
        )

        self.second = Arrays.objects.create(
            stored_array=[{
                "a": 3,
                "b": 4
            },
            {
                "a": 5,
                "b": 6
            }]
        )

    def test_index(self):
        print('================' + self._testMethodName + '================')
        response = client.get(reverse('index'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_arrays(self):
        print('================' + self._testMethodName + '================')
        # get API response
        response = client.get(reverse('get_all_arrays'))
        print(response.data)
        # get data from db
        arrays = Arrays.objects.all()
        serializer = ArraysSerializer(arrays, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_valid_get_array(self):
        print('================' + self._testMethodName + '================')
        response = client.get(reverse('get_single_array',
                                      kwargs={'pk': self.first.pk}))
        print(response.data)
        array = Arrays.objects.get(pk=self.first.pk)
        serializer = ArraysSerializer(array)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_get_array(self):
        response = client.get(reverse('get_single_array', kwargs={'pk': 333}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewArrayTest(TestCase):
    """ Test module for inserting a new array"""

    def setUp(self):
        """ send data by using web interface
            "stored_array": [
                {
                    "b": 2,
                    "a": 1
                }
            ]
        """
        self.valid_data1 = {
            'stored_array': '[{"a": 1, "b": 2}]'
        }
        self.valid_data2 = {
            'stored_array': '[{"a": 1, "b": 2}, {"a": 3, "b": 4}]'
        }
        self.invalid_data = {
            'stored_array': ''
        }

    def test_valid_post_new_array(self):
        print('================' + self._testMethodName + '================')
        response = client.post(
            reverse('post_new_array'),
            data=json.dumps(self.valid_data1),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = client.post(
            reverse('post_new_array'),
            data=json.dumps(self.valid_data2),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_invalid_post_array(self):
        print('================' + self._testMethodName + '================')
        response = client.post(
            reverse('post_new_array'),
            data=json.dumps(self.invalid_data),
            content_type='application/json'
        )
        print(response.data)
        # self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
