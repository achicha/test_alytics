from django.test import TestCase
from ..models import Arrays


class NumberArraysModelTest(TestCase):
    """ Test module for NumberArrays Model """

    def setUp(self):
        Arrays.objects.create(
            stored_array=[{
                "a": 1,
                "b": 2
            }]
        )

        Arrays.objects.create(
            stored_array=[{
                "a": 3,
                "b": 4
            },
            {
                "a": 5,
                "b": 6
            }]
        )

    def test_get_numbers(self):
        print('================' + self._testMethodName + '================')
        arr1 = Arrays.objects.get(id=1).stored_array
        print(arr1)
        self.assertEqual(arr1[0]['a'], 1)
        self.assertEqual(arr1[0]['b'], 2)

        arr2 = Arrays.objects.get(id=2).stored_array
        print(arr2)
        self.assertEqual(arr2[0]['a'], 3)
        self.assertEqual(arr2[0]['b'], 4)
        self.assertEqual(arr2[1]['a'], 5)
        self.assertEqual(arr2[1]['b'], 6)

