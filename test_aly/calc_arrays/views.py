from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render_to_response, redirect
from .models import Arrays, Errors
from .serializers import ArraysSerializer, ErrorsSerializer
from .tasks import task_fetch, task_test_func, task_save_result, add, task_log_error, task_update_status
from celery import chain, group


def _get_single_array_or_error(pk):
    try:
        arr = Arrays.objects.get(pk=pk)
        return arr
    except Arrays.DoesNotExist:
        return ''


@api_view(['GET'])
def get_single_array(request, pk):
    """get data from DB for single array by key"""
    arr = _get_single_array_or_error(pk)
    # get a single array
    if arr and request.method == 'GET':
        serializer = ArraysSerializer(arr)
        return Response(serializer.data)
    return Response({}, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET', 'POST'])
def post_new_array(request):
    """post a new array

    Example:

    {
        "stored_array": [
            {
                "b": 2,
                "a": 1
            }
        ]
    }
    """
    if request.method == 'POST':
        data = {
            'stored_array': request.data.get('stored_array')
        }
        serializer = ArraysSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'GET':
        return Response()


@api_view(['GET'])
def get_all_arrays(request):
    """get all arrays"""
    arrays = Arrays.objects.all()
    serializer = ArraysSerializer(arrays, many=True)
    return Response(serializer.data)


# @api_view(['GET'])
# def get_array_result(request, pk):
#     # get last result for single array
#     if request.method == 'GET':
#         return Response({})


@api_view(['GET'])
def get_errors(request):
    """exceptions list"""
    errors_list = Errors.objects.all()
    serializer = ErrorsSerializer(errors_list, many=True)
    return Response(serializer.data)


def index(request):
    """main page"""
    last_status = False
    if request.method == 'GET':
        # if the button was pushed
        if request.GET.get('mybtn'):
            #arrays = Arrays.objects.all()
            arrs = [_get_single_array_or_error(i.id) for i in Arrays.objects.all()]

            job = group([chain(task_fetch.s(i.stored_array[0]),
                               # todo on_error is not work here?
                               task_test_func.s(), #.on_error(task_log_error('error'))
                               task_save_result.s(i.id),
                               task_update_status.s(last_status)
                               ) for i in arrs])
            res = job.apply_async()
            print(res.as_tuple())

            return redirect("/api/v1/arrays/")
        return render_to_response('main.html', {'result_status': last_status})
