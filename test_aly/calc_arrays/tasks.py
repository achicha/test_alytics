from calc_arrays.serializers import ErrorsSerializer
from test_aly.celery_app import app
from .models import Arrays

@app.task
def add(x, y):
    return x + y


@app.task(time_limit=20, soft_time_limit=10, queue='first')
def task_fetch(dict_obj):
    """get data from DB (dict) -> json"""
    import json
    js_data = json.dumps(dict_obj)
    return js_data


@app.task(time_limit=10, soft_time_limit=5, queue='second')
def task_test_func(fetched_json_data):
    """calculate result(or error) for 1 dataset -> json
    """
    import json
    try:
        d = json.loads(fetched_json_data)
        return {'result': d['a'] + d['b'], 'error': None}
    except BaseException as e:
        import traceback
        return {'result': None, 'error': str(traceback.format_exc())}


@app.task(queue='third')
def task_save_result(result_json, array_id):
    """save result to DB"""
    # todo: try to handle this without importing Arrays and ErrorsSerializer
    try:
        arr = Arrays.objects.get(pk=array_id)
    except Arrays.DoesNotExist:
        return {'result_status': False}

    if arr and result_json['result']:
        arr.result = result_json['result']
        arr.save()
        return {'result_status': True}
    elif result_json['error']:
        data = {'error_stack': result_json['error'],
                'arrays': array_id}
        serializer = ErrorsSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
        return {'result_status': False}
    else:
        return {'result_status': False}


@app.task
def task_log_error(data):
    return str(data)


@app.task
def task_update_status(result_status, status_arg):
    """update last calc status: True/False"""
    # todo store status in cache like memcached/redis?
    status_arg = result_status['result_status']
    return status_arg
